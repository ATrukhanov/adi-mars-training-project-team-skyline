import React from 'react';

import MovieCard from './index';
import * as props from 'src/_mocks_/propsMock';

export default { title: 'Movie Card' };

export const defaultView = () => (
  <MovieCard
    title={props.propsWithImg.title}
    genres={props.propsWithImg.genre}
    year={props.propsWithImg.date}
    imagePath={props.propsWithImg.imagePath}
  />
);

export const placeholderView = () => (
  <MovieCard
    title={props.propsWithDefaultImg.title}
    genres={props.propsWithDefaultImg.genre}
    year={props.propsWithDefaultImg.date}
    imagePath={props.propsWithDefaultImg.imagePath}
  />
);
