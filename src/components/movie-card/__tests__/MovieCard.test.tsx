import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import MovieCard from '../';
import * as props from 'src/_mocks_/propsMock.json';

describe('<MovieCard />', () => {
  test('matches snapshot', () => {
    const wrapper = shallow(
      <MovieCard
        title={props.propsWithImg.title}
        genres={props.propsWithImg.genre}
        year={props.propsWithImg.date}
        imagePath={props.propsWithImg.imagePath}
      />
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
