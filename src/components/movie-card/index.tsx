import React from 'react';

import PLACEHOLDER_IMAGE from 'src/constants/placeholderImage';
import getYearFromDate from 'src/utils/getYearFromDate';
import formatGenres from 'src/utils/formatGenres';

import styles from './MovieCard.module.css';

interface MovieCardProps {
  title: string;
  genres: string[];
  year: string;
  imagePath?: string;
}

function MovieCard({ imagePath = PLACEHOLDER_IMAGE, title, genres, year }: MovieCardProps) {
  return (
    <div className={styles.movieCard}>
      <div className={styles.movieCardImageWrapper}>
        <img className={styles.movieCardImage} src={imagePath} alt={`movie ${title} image`} />
      </div>
      <div>
        <div className={styles.mainInfo}>
          <div className={styles.title}>{title}</div>
          <div className={styles.year}>{getYearFromDate(year)}</div>
        </div>
        <div className={styles.genreList}>{formatGenres(genres)}</div>
      </div>
    </div>
  );
}

export default MovieCard;
