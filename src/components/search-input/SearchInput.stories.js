import React from 'react';

import SearchInput from './';

export default { title: 'Search Input' };

export const defaultView = () => <SearchInput />;
