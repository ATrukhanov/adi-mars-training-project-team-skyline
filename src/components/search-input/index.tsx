import React from 'react';

import styles from './SearchInput.module.css';

export interface SearchInputProps {
  onChange(value?: string): void;
  onKeyPress(value?: string): void;
  value?: string;
  placeholder?: string;
}

export default function Index(props: SearchInputProps) {
  const { value, placeholder = '', onChange, onKeyPress } = props;

  function handleInputChange({ currentTarget: { value = '' } }: React.ChangeEvent<HTMLInputElement>) {
    onChange(value);
  }

  function handleEnterKeyDown({ currentTarget: { value = '' }, key }: React.KeyboardEvent<HTMLInputElement>) {
    if (key === 'Enter') {
      onKeyPress(value);
    }
  }

  return (
    <input
      className={styles['inputField']}
      type="text"
      placeholder={placeholder}
      value={value}
      onChange={handleInputChange}
      onKeyPress={handleEnterKeyDown}
    />
  );
}
