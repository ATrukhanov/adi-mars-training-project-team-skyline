import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import toJson from 'enzyme-to-json';

import Index from '../index';

type Props = {
  onChange(event: { currentTarget: { value: string } }): void;
  onKeyPress(event: { currentTarget: { value: string } }): void;
};
const props = {
  onChange: jest.fn(),
  onKeyPress: jest.fn()
};

const eventAnotherKeyPress = {
  key: 'Another key',
  currentTarget: {
    value: ''
  }
} as React.KeyboardEvent<HTMLInputElement>;

const eventKeyPress = {
  key: 'Enter',
  currentTarget: {
    value: 'some text'
  }
} as React.KeyboardEvent<HTMLInputElement>;

describe('<Index />', () => {
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(<Index {...props} />);
  });

  it('should match snapshot', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should be called when change input', () => {
    (wrapper.props() as Props).onChange({ currentTarget: { value: 'qwerty' } });
    expect(props.onChange).toBeCalled();
  });

  it('should not be called when press another key', () => {
    (wrapper.props() as Props).onKeyPress(eventAnotherKeyPress);
    expect(props.onKeyPress).toBeCalledTimes(0);
  });

  it('should be called when press enter key', () => {
    (wrapper.props() as Props).onKeyPress(eventKeyPress);
    expect(props.onKeyPress).toBeCalled();
  });
});
