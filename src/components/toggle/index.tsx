import React from 'react';
import { RadioGroup, Radio } from 'react-radio-group';

import styles from './toggle.module.css';

interface ToggleProps {
  title: string;
  values: string[];
  active: string;
  changeActive(v: string): void;
}

export default function Toggle({ title, values, active, changeActive }: ToggleProps) {
  return (
    <div className={styles.toggleContainer}>
      <span className={styles.toggleTittle}>{title}</span>
      <RadioGroup name="toggle" selectedValue={active} onChange={changeActive}>
        {values.map((value, idx) => (
          <label className={`${styles.toggleElement} ${active === value ? styles.checked : null}`} key={idx}>
            {value}
            <Radio className={styles.toggleRadio} value={value} />
          </label>
        ))}
      </RadioGroup>
    </div>
  );
}
