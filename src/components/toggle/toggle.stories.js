import React, { useState, Children } from 'react';

import Toggle from './index';

export default { title: 'Toggle Element' };


function ToggleWrapper (props){

    const [active, setActive] = useState(props.activeItem);

    const changeActive = function(value) {
        setActive(value);
    }

    const toggleData = {
        title: props.title,
        values: props.values,
        active,
        changeActive
    };

    const childrenWithProps = React.Children.map(props.children, child =>
        React.cloneElement(child, {...toggleData}, )
    );

    return (
        <>
            {childrenWithProps}
        </>
    )
}

const firstActiveProps = {
    title: 'Search by',
    values: ['Tittle', 'Genre'],
    activeItem: 'Tittle'
}
export const firstActiveView = () => (
    <ToggleWrapper {...firstActiveProps}>
        <Toggle />
    </ToggleWrapper>
);

const secondActiveProps = {
    title: 'Search by',
    values: ['Tittle', 'Genre'],
    activeItem: 'Genre'
}

export const secondActiveView = () => (
    <ToggleWrapper {...secondActiveProps}>
        <Toggle />
    </ToggleWrapper>
);

const threeItemsProps = {
    title: 'Search by',
    values: ['Tittle', 'Some', 'Genre'],
    activeItem: 'Some'
}

export const threeItemsView = () => (
    <ToggleWrapper {...threeItemsProps}>
        <Toggle />
    </ToggleWrapper>
);
