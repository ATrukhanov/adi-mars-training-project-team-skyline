import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Toggle from '../';

const firstValue = 'Some';
const secondValue = 'Another';

const props = {
  title: 'Search',
  values: [firstValue, secondValue],
  active: firstValue,
  changeActive: jest.fn()
};

describe('<Toggle />', () => {
  const component = shallow(<Toggle {...props} />);

  it('should match the snapshot ', () => {
    expect(toJson(component)).toMatchSnapshot();
  });

  it('should call changeActive function', () => {
    const radioGroup = component.find('RadioGroup');
    radioGroup.simulate('change');
    expect(props.changeActive).toHaveBeenCalled();
  });
});
