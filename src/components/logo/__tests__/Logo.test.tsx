import React from 'react';
import { shallow } from 'enzyme';

import Logo from '../';

const testImagePath = '/Netflix_logo.png';

describe('<Logo />', () => {
  it('has a right href and image', () => {
    const wrapper = shallow(<Logo />);
    expect(wrapper.prop('href')).toEqual('/');
    expect(wrapper.find('img').prop('src')).toEqual(testImagePath);
  });
});
