import React, { ReactElement } from 'react';

import Logo from './index';

export default { title: 'Logo' };

export const defaultView = (): ReactElement => <Logo />;
