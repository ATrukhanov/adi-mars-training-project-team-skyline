import React, { ReactElement } from 'react';

import styles from './Logo.module.css';

function Logo(): ReactElement {
  return (
    <a href="/" className={styles.logoLink} data-testid="logo-link">
      <img src="/Netflix_logo.png" alt="Logo image" className={styles.logoImage} />
    </a>
  );
}

export default Logo;
