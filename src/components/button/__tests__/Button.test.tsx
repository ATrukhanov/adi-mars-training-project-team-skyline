import React from 'react';
import { shallow } from 'enzyme';

import Button from '../index';

describe('<Button />', () => {
  const props = {
    title: 'Search',
    className: 'buttonSearch',
    onClick: jest.fn()
  };

  const component = shallow(<Button {...props} />);

  it('properly handle click event', () => {
    component.find('button').simulate('click');
    expect(props.onClick).toHaveBeenCalled();
  });
});
