import React, { MouseEvent } from 'react';

import styles from './button.module.css';

type ButtonType = JSX.IntrinsicElements['button']['type'];

interface ButtonProps {
  title: string;
  className: string;
  onClick(event: MouseEvent): void;
  type?: ButtonType;
  disabled?: boolean;
}

export default function Button(props: ButtonProps) {
  const { title, className, onClick, type = 'button', disabled = false } = props;

  return (
    <button className={`${styles.button} ${styles[className]}`} onClick={onClick} type={type} disabled={disabled}>
      {title}
    </button>
  );
}
