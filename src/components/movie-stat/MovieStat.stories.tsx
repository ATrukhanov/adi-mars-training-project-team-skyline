import React from 'react';

import MovieStat from './MovieStat';

import { STAT_TYPES } from 'src/constants/StatTypes';

export default { title: 'Stat Component' };

export const defaultView = () => <MovieStat type={STAT_TYPES.year} value={1994} />;
