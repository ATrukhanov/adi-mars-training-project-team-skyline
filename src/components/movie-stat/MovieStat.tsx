import React from 'react';

import styles from './MovieStat.module.css';

import { STAT_TYPES as statType } from 'src/constants/StatTypes';

interface MovieStatProps {
  type: statType;
  value: number;
}

export default function MovieStat({ type, value }: MovieStatProps) {
  return (
    <div className={styles.movieStatContainer}>
      <span className={styles.movieStatValue}>{value} </span>
      <span className={styles.movieStatType}>{statType[type]}</span>
    </div>
  );
}
