import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import toJson from 'enzyme-to-json';

import { STAT_TYPES as statType } from 'src/constants/StatTypes';
import MovieStat from '../MovieStat';

const props = {
  type: statType.year,
  value: 1994
};

describe('<MovieStat />', () => {
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(<MovieStat {...props} />);
  });

  it('should match snapshot', () => {
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should display min value for duration type', () => {
    wrapper.setProps({ type: statType.min, value: 123 });
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
