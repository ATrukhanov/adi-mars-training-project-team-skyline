import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import MovieInfo from '../';
import * as props from 'src/_mocks_/propsMock.json';

describe('<MovieInfo />', () => {
  const component = shallow(<MovieInfo {...props.propsWithImg} />);

  it('should render component properly', () => {
    expect(toJson(component)).toMatchSnapshot();
  });
});
