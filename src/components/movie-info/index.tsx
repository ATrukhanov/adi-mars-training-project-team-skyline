import React, { ReactElement } from 'react';

import MovieStat from '../movie-stat/MovieStat';
import formatGenres from 'src/utils/formatGenres';
import getYearFromDate from 'src/utils/getYearFromDate';
import { STAT_TYPES as statTypes } from 'src/constants/StatTypes';
import PLACEHOLDER_IMAGE_PATH from 'src/constants/placeholderImage';

import styles from './movieInfo.module.css';

interface MovieInfoProps {
  title: string;
  rating: number;
  genre: string[];
  date: string;
  duration: number;
  description: string;
  imagePath?: string;
}

export default function MovieInfo(props: MovieInfoProps): ReactElement {
  const { title, rating, genre, date, duration, description, imagePath = PLACEHOLDER_IMAGE_PATH } = props;

  return (
    <div className={styles.movieInfoWrapper}>
      <div className={styles.movieImage}>
        <img src={imagePath} alt={`${title} poster`} />
      </div>
      <div className={styles.movieInfo}>
        <div className={styles.movieInfoHeader}>
          <h1 className={styles.movieTitle}>{title}</h1>
          <div className={styles.movieRating}>{rating}</div>
        </div>
        <div className={styles.movieGenre}>{formatGenres(genre)}</div>
        <div className={styles.movieStatistic}>
          <MovieStat type={statTypes.year} value={getYearFromDate(date)} />
          <MovieStat type={statTypes.min} value={duration} />
        </div>
        <div className={styles.movieDescription}>{description}</div>
      </div>
    </div>
  );
}
