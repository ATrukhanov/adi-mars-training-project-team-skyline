import React from 'react';
import { storiesOf } from '@storybook/react';

import MovieInfo from './';
import * as props from 'src/_mocks_/propsMock.json';

storiesOf('MovieInfo', module)
  .add('with poster', () => <MovieInfo {...props.propsWithImg} />)
  .add('without poster', () => <MovieInfo {...props.propsWithDefaultImg} />);
