export default function getYearFromDate(date: string) {
  const getYear = /\d{4}/i;
  const year = getYear.exec(date) as string[];
  return Number(year[0]);
}
