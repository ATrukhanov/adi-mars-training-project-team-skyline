export default function formatGenres(genresArr: string[]) {
  return genresArr.join(' & ');
}
