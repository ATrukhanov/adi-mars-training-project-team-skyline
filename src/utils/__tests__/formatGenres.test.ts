import formatGenres from 'src/utils/formatGenres';

describe('formatGenres test', () => {
  it('should format genres from array', () => {
    const genresArr: string[] = ['Fantasy', 'Adventure', 'Science Fiction'];
    const expectedResult = 'Fantasy & Adventure & Science Fiction';
    expect(formatGenres(genresArr)).toEqual(expectedResult);
  });

  it('should return empty string', () => {
    const genresArr: string[] = [];
    const expectedResult = '';
    expect(formatGenres(genresArr)).toEqual(expectedResult);
  });

  it('fail to format genres', () => {
    const genresArr: string[] = ['Fantasy', 'Adventure', 'Science Fiction'];
    const expectedResult = 'Fantasy, Adventure, Science Fiction';
    expect(formatGenres(genresArr)).not.toBe(expectedResult);
  });
});
