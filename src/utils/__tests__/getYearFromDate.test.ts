import getYearFromDate from 'src/utils/getYearFromDate';

describe('getYearFromDate', () => {
  describe('Should get year from date', () => {
    it('When year is declared first', () => {
      const dateString = '2017-12-13';
      const expectedResult = 2017;
      expect(getYearFromDate(dateString)).toEqual(expectedResult);
    });

    it('When year is declared last', () => {
      const dateString = '13-12-2017';
      const expectedResult = 2017;
      expect(getYearFromDate(dateString)).toEqual(expectedResult);
    });
  });

  describe('Should not get year from date', function() {
    it('Should not get date or month from date', () => {
      const dateString = '2017-12-13';
      expect(getYearFromDate(dateString)).not.toBe(12);
      expect(getYearFromDate(dateString)).not.toBe(13);
    });
  });
});
